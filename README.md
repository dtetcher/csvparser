**Configure database connection**

*Follow this steps to successfully set up your database connection*
*Writing this I believe that you have already installed MySQL client and server on your machine.*

---

## Create MySQL database

1. Enter MySQL interactive mode.
2. Execute this command **CREATE DATABASE *db_name*;**
*Where db name is your database name*

---

## Create MySQL user

1. Execute **CREATE USER [IF NOT EXISTS] *account_name* IDENTIFIED BY '*password*';**.
*Where account_name is your user's name and 'password' is your secured password*
2. Grant all permissions on database to recently created user.
3. Execute **GRANT ALL PRIVILEGES ON *db_name*.* TO '*account_name*'@'localhost';**.

---

## Set MySQL credentials in Django project

1. Open *settings.py* file under main app folder(CSVParser).
2. Find DATABASES section
3. Set it like this **'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': '*db_name*',
        'USER': '*your_account*',
        'PASSWORD': '*password*',
        'HOST': 'localhost',
        'PORT': '80'**
    }

---

## Make migration

1. Find *manage.py* file.
*It's located under Django project folder*
2. Make your migrations **manage.py makemigrations**
3. Then migrate changes to MySQL db **manage.py migrate**

---

**Now you can simply start web application**
*Command - manage.py runserver*